(define (true a b) a)
(define (flase a b) b)

(define (isZero n)(= n 0))
(define (LT m n)(< m n))
(define sum 0)
(define i 0)

(define (or m n)(true m (false m n)))
(define m 0)
(define n 11)

(define (to_pass sum j n) ( if (< j n) (+ (+ j sum) (to_pass sum(+ 1 j) n))(+ sum j)))

(define (uselessFunc m n)
(if (or (isZero m) (LT m n)) 
    (to_pass sum i n)
    (+ n m)
    )
   )
  
(uselessFunc m n)